# Build

Do not use `Makefile`, since it is widely broken.
Or it does not work for `make` of Mac OS X.

* `opam switch create ./ ocaml-base-compiler.4.06.1`
* `opam install merlin tuareg`
* `scripts/setup_ligo_opam_repository.sh`
* `scripts/install_ligo_with_dependencies.sh`
* `scripts/build_ligo_local.sh` or `dune build -p ligo`

