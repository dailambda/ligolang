---
id: editor-support
title: Editor Support
---

Good editor support is the basic component of proper development experience - currently, we provide support for VSCode via an [extension](https://marketplace.visualstudio.com/items?itemName=Brice.ligo).

Currently the extension supports Pascaligo for syntax highlighting (work in progress). But it aims to support debug, gas optimization, dry run and other relevant features in the near future.