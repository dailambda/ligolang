---
id: version-next-language-basics-variables
title: Variables
original_id: language-basics-variables
---

## Defining a variable

<!--DOCUSAURUS_CODE_TABS-->
<!--Pascaligo-->
```Pascal
// int
const four : int = 4;

// string
const name : string = "John Doe";
```

<!--END_DOCUSAURUS_CODE_TABS-->