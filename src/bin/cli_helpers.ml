open Trace

let toplevel x =
  match x with
  | Trace.Ok ((), annotations) -> ignore annotations; ()
  | Error ss ->
     let e = ss () in
     Format.eprintf "Error: @[%a@]@." Ligo.Display.error_pp e;
     if Sys.getenv_opt "LIGO_HACK" <> None then Format.eprintf "Error: @[%a@]@." Yojson.Basic.pp e;
     prerr_endline "exitting...";
     exit 1
