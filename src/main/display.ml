open Trace

let error_pp out (e : error) =
  let open JSON_string_utils in
  let message =
    let opt = e |> member "message" |> string in
    match opt with
    | Some msg -> ": " ^ msg
    | None -> "" in
  let error_code =
    let error_code = e |> member "error_code" in
    match error_code with
    | `Null -> ""
    | _ -> " (" ^ (J.to_string error_code) ^ ")" in
  let title =
    let opt = e |> member "title" |> string in
    Option.unopt ~default:"" opt in
  let data =
    let data = e |> member "data" in
    match data with
    | `Null -> ""
    | _ -> " " ^ (J.to_string data) ^ "\n" in
  let infos =
    let infos = e |> member "infos" in
    match infos with
    | `List lst -> lst
    | `Null -> []
    | x -> [ x ] in

  let module Loc = struct
      type t = 
        | String of string
        | Loc of int * (int * int)
               
      let parse = function
        | "in file \"\", line 0, characters 0-0" -> None
        | "generated" -> None
        | s ->
           match String.split_on_char ',' s with
           | ["in file \"\""; s1; s2] ->
              begin match Scanf.sscanf s1 " line %d" (fun x -> Some x) with
              | exception e -> failwith (s1 ^ " : " ^ Printexc.to_string e)
              | None -> None
              | Some line ->
                 match Scanf.sscanf s2 " characters %d-%d" (fun x y -> Some (x,y)) with
              | exception e -> failwith (s1 ^ " : " ^ Printexc.to_string e)
                 | None -> None
                 | Some (c1, c2) -> Some (Loc (line, (c1, c2)))
              end
           | _ -> Some (String s)
                
      let parse_opt = function
        | None -> None
        | Some s -> parse s
           
      let is_more_detailed t1 t2 = match t1, t2 with
        | String _, _ | _, String _ -> false
        | Loc (l, (c1, c2)), Loc (l', (c1', c2')) ->
           if l <> l' then false
           else c1' <= c1 && c2 <= c2'
          
      let to_string = function
        | String s -> s
        | Loc (line, (c1, c2)) -> Printf.sprintf "line %d, characters %d-%d" line c1 c2
    end 
  in

  let opt = e |> member "data" |> member "location" |> string |> Loc.parse_opt in
  let aux prec cur =
    let l = cur |> member "data" |> member "location" |> string |> Loc.parse_opt in
    match prec, l with
    | Some p, Some l when Loc.is_more_detailed p l -> Some p
    | Some p, Some l when Loc.is_more_detailed l p -> Some l
    | Some p, _ -> Some p
    | None, x -> x
  in
  let location = 
    match opt with
    | Some l -> Loc.to_string l ^ ". "
    | None ->
       match List.fold_left aux None infos with
       | None -> ""
       | Some s -> "around " ^ Loc.to_string s ^ ". "
  in
  let print x = Format.fprintf out x in
  print "%s%s%s%s%s" location title error_code message data
  (* Format.fprintf out "%s%s%s.\n%s%s" title error_code message data infos *)
